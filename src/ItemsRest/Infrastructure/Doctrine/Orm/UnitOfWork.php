<?php
declare(strict_types=1);

namespace ItemsRest\Infrastructure\Doctrine\Orm;

use Doctrine\ORM\EntityManagerInterface;

class UnitOfWork
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function commit()
    {
        $this->em->flush();
    }
}
<?php
declare(strict_types = 1);

namespace ItemsRest\Infrastructure\Doctrine\Orm;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use ItemsRest\NotFoundException;

abstract class DoctrineRepository
{
    /** @var EntityManagerInterface */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    protected function createQueryBuilder(string $alias, string $indexBy = null): QueryBuilder
    {
        return $this->em->createQueryBuilder()
            ->select($alias)
            ->from($this->entityName(), $alias, $indexBy);
    }

    protected function doGet(int $id)
    {
        $qb = $this->createQueryBuilder('e')
            ->where('e.id = :id')
            ->setParameter('id', $id);

        return $this->getSingleResult($qb);
    }

    protected function doAdd($entity)
    {
        $this->em->persist($entity);
    }

    protected function doRemove($entity)
    {
        $this->em->remove($entity);
    }

    protected function getSingleResult(QueryBuilder $qb)
    {
        try {
            return $qb->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            throw new NotFoundException($e->getMessage(), $e->getCode());
        }
    }

    abstract protected function entityName(): string;
}

<?php
declare(strict_types=1);

namespace ItemsRest\Infrastructure\Doctrine\Orm;

use ItemsRest\Domain\Item;
use ItemsRest\Domain\ItemRepository;

class ItemDoctrineRepository extends DoctrineRepository implements ItemRepository
{
    public function add(Item $item): void
    {
        $this->doAdd($item);
    }

    public function get(int $id): Item
    {
        return $this->doGet($id);
    }

    public function remove(Item $item): void
    {
        $this->doRemove($item);
    }

    protected function entityName(): string
    {
        return Item::class;
    }
}
<?php
declare(strict_types=1);

namespace ItemsRest\Infrastructure\Doctrine\Dbal;

use Doctrine\DBAL\Query\QueryBuilder;
use ItemsRest\Application\Query\ItemQuery;
use ReadModel\Bridge\Doctrine\Query\WalkableDbalQuery;
use ReadModel\Filters\Filters;
use ReadModel\Paginator;

class ItemDbalQuery extends WalkableDbalQuery implements ItemQuery
{
    protected $typeMapping = [
        'id' => 'int',
        'amount' => 'int'
    ];

    public function find(string $id): array
    {
        $qb = $this->createQueryBuilder()
            ->where('id = :id')
            ->setParameter('id', intval($id));

        return $this->getSingleResult($qb);
    }

    public function findAll(Filters $filters): Paginator
    {
        $qb = $this->createQueryBuilder();
        $this->andWhereMinAmount($qb, $filters->useFilter('min_amount'));
        $this->andWhereMaxAmount($qb, $filters->useFilter('max_amount'));

        return $this->createPaginatorForFilters($qb, $filters);
    }

    protected function createQueryBuilder(): QueryBuilder
    {
        $qb = parent::createQueryBuilder()
            ->select('id, name, amount')
            ->from('item');

        return $qb;
    }

    private function andWhereMinAmount(QueryBuilder $qb, int $amount = null): void
    {
        if ($amount > 0) {
            $qb->andWhere('amount >= :min_amount');
            $qb->setParameter('min_amount', $amount);
        }
    }

    private function andWhereMaxAmount(QueryBuilder $qb, int $amount = null): void
    {
        if ($amount !== null) {
            $qb->andWhere('amount <= :max_amount');
            $qb->setParameter('max_amount', $amount);
        }
    }
}
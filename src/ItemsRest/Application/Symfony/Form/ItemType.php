<?php
declare(strict_types=1);

namespace ItemsRest\Application\Symfony\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;

class ItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('name', TextType::class, [
                'constraints' => new NotBlank(['message' => 'name.not_blank'])
            ])
            ->add('amount', IntegerType::class, [
                'constraints' => [
                    new NotNull(['message' => 'amount.not_blank']),
                    new Type(['type' => 'numeric', 'message' => 'amount.not_numeric']),
                    new GreaterThanOrEqual(['value' => 0, 'message' => 'amount.not_negative'])
                ]
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true
        ]);
    }
}
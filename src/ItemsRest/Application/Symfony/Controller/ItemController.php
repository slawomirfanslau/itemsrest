<?php
declare(strict_types=1);

namespace ItemsRest\Application\Symfony\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use ItemsRest\Application\Symfony\Form\ItemType;
use ItemsRest\Domain\Items;
use ItemsRest\Infrastructure\Doctrine\Dbal\ItemDbalQuery;
use ItemsRest\Infrastructure\Doctrine\Orm\UnitOfWork;
use ReadModel\Bridge\Symfony\RequestFiltersBuilderFactory;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

class ItemController extends FOSRestController
{
    use RequestFiltersBuilderFactory;

    /**
     * @ApiDoc(
     *  description="Creates item",
     *  section="Item",
     *  parameters={
     *      {"name"="name", "dataType"="string", "required"=true},
     *      {"name"="amount", "dataType"="integer", "required"=true}
     *  }
     * )
     *
     * @Rest\Post("/items")
     * @Rest\View(statusCode=201)
     * @param Request $request
     * @return array|Response
     */
    public function postItemAction(Request $request)
    {
        try {
            $data = $this->submitData($request->request->all());
        } catch (\Exception $ex) {
            return new Response('validation errors', 400);
        }

        $item = $this->get(Items::class)->create($data['name'], intval($data['amount']));
        $this->get(UnitOfWork::class)->commit();

        return ['item' => $this->get(ItemDbalQuery::class)->find((string)$item->id())];
    }

    /**
     * @ApiDoc(
     *  description="Updates item",
     *  section="Item",
     *  parameters={
     *      {"name"="name", "dataType"="string", "required"=true},
     *      {"name"="amount", "dataType"="integer", "required"=true}
     *  }
     * )
     *
     * @Rest\Put("/items/{id}", requirements={"id":"\d+"})
     * @Rest\View(statusCode=200)
     * @param int $id
     * @param Request $request
     * @return array|Response
     */
    public function putItemAction(int $id, Request $request)
    {
        try {
            $data = $this->submitData($request->request->all());
        } catch (\Exception $ex) {
            return new Response('validation errors', 400);
        }

        $this->get(Items::class)->edit($id, $data['name'], intval($data['amount']));
        $this->get(UnitOfWork::class)->commit();

        return ['item' => $this->get(ItemDbalQuery::class)->find((string)$id)];
    }

    /**
     * @ApiDoc(
     *  description="Updates item",
     *  section="Item"
     * )
     *
     * @Rest\Delete("/items/{id}", requirements={"id":"\d+"})
     * @Rest\View(statusCode=204)
     * @param int $id
     */
    public function deleteItemAction(int $id)
    {
        $this->get(Items::class)->remove($id);
        $this->get(UnitOfWork::class)->commit();
    }


    /**
     * @ApiDoc(
     *  description="Finds items",
     *  section="Item",
     *  parameters={
     *    {"name"="min_amount", "dataType"="integer", "required"=false},
     *    {"name"="max_amount", "dataType"="integer", "required"=false}
     *  }
     * )
     *
     * @Rest\Get("/items")
     * @param Request $request
     * @return array
     */
    public function getItemsAction(Request $request)
    {
        $filters = $this->createFiltersBuilder($request, null, 20)
            ->addIntegerFilter('min_amount')
            ->addIntegerFilter('max_amount')
            ->buildFilters();

        $paginator = $this->get(ItemDbalQuery::class)->findAll($filters);

        return [
            'items' => $paginator->getResults(),
            'meta' => $paginator->getMeta()
        ];
    }

    /**
     * @ApiDoc(
     *  description="Returns item by id",
     *  section="Item"
     * )
     *
     * @Rest\Get("/items/{id}", requirements={"id":"\d+"})
     * @param string $id
     * @return array
     */
    public function getItemAction(string $id)
    {
        return ['item' => $this->get(ItemDbalQuery::class)->find($id)];
    }

    private function submitData(array $data): array
    {
        $form = $this->createForm(ItemType::class);
        $form->submit($data);

        if (!$form->isValid()) {
            throw new \Exception();
        }

        return $form->getData();
    }
}
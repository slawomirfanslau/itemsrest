<?php
declare(strict_types=1);

namespace ItemsRest\Application\Query;

use ReadModel\Filters\Filters;
use ReadModel\Paginator;

interface ItemQuery
{
    public function find(string $id): array;

    public function findAll(Filters $filters): Paginator;
}
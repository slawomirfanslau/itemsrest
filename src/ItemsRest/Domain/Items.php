<?php
declare(strict_types=1);

namespace ItemsRest\Domain;

class Items
{
    /** @var ItemRepository */
    private $repository;

    public function __construct(ItemRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(string $name, int $amount): Item
    {
        $item = new Item($name, $amount);
        $this->repository->add($item);

        return $item;
    }

    public function edit(int $id, string $name, int $amount): void
    {
        $item = $this->repository->get($id);
        $item->changeName($name);
        $item->changeAmount($amount);
    }

    public function remove(int $id): void
    {
        $item = $this->repository->get($id);
        $this->repository->remove($item);
    }
}
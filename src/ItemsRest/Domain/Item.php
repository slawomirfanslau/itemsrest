<?php
declare(strict_types=1);

namespace ItemsRest\Domain;

use Assert\Assertion;

class Item
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var int */
    private $amount;

    public function __construct(string $name, int $amount)
    {
        $this->changeName($name);
        $this->changeAmount($amount);
    }

    public function changeName(string $name): void
    {
        Assertion::notEmpty($name);

        $this->name = $name;
    }

    public function changeAmount(int $amount): void
    {
        Assertion::greaterOrEqualThan($amount, 0);

        $this->amount = $amount;
    }

    public function id(): int
    {
        return $this->id;
    }
}
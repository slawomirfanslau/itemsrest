<?php
declare(strict_types=1);

namespace ItemsRest\Domain;

interface ItemRepository
{
    public function add(Item $item): void;

    public function get(int $id): Item;

    public function remove(Item $item): void;
}